# Second question (ANOVA)
# Rcmdr needed by mergeRows
# lmtest needed by dwtest

# Reset data
rm(list = ls())

library("lmtest")

Norm_v1 = rnorm(200, mean = 0, sd = 1)
Norm_v2 = rnorm(200, mean = 10, sd = 1)
Norm_v3 = rnorm(200, mean = 0, sd = 1)

Norm_v1n = data.frame(x1 = Norm_v1, x2 = "v1")
Norm_v2n = data.frame(x1 = Norm_v2, x2 = "v2")
Norm_v3n = data.frame(x1 = Norm_v3, x2 = "v3")

data = mergeRows(Norm_v1n, Norm_v2n, common.only = FALSE)
data = mergeRows(as.data.frame(data), Norm_v3n, common.only = FALSE)

# ANOVA:
# H0: the F statistic has an "F" distribution with:
#    K-1 and N-K degrees of freedom (N is the total number of observations)
AnovaModel.1 <- aov(x1 ~ x2, data = data)

summary(AnovaModel.1)

# Sample execution:
#              Df Sum Sq Mean Sq F value Pr(>F)
# x2            2  13161    6581    6371 <2e-16 ***
# Residuals   597    617       1
#
# As the p-value (<2e-16) is less than the significance level 0.05,
# we can conclude that there are significant differences between
# the means of the groups.

Boxplot(x1~x2, data = data, id.method = "y")

# Test the ANOVA assumptions:
# - The observations within each sample must be independent: Durbin Watson test
# - The populations from which the samples are selected must be normal: Shapiro test
# - The populations from which the samples are selected must have equal variances
#   (homogeneity of variance): Breusch Pagan test

# Durbin Watson test
dwtest(AnovaModel.1, alternative = "two.sided")
# As the p-value is > 0.05, the observations within each sample
# can be considered to be independent.

# Shapiro test
shapiro.test(Norm_v1)
shapiro.test(Norm_v2)
shapiro.test(Norm_v3)
# All the p-values are > 0.05, so the populations from which the samples
# are selected follow a normal distribution.

# Breusch Pagan test
lmtest::bptest(AnovaModel.1)
# As the p-value is > 0.05, the different samples can be considered
# to have equal variances, also known as homoscedasticity.

############# Second part of exercise #############

# geomorphology
data(geomorphology, package="FactoMineR")

#
drift_block_size_aov <- aov(Block.size.median ~ Drift, data = geomorphology)
summary(drift_block_size_aov)
# p-value = 0.606 > 0.05, so we can't refuse H0 which means that
# Drift is a factor that defines different block sizes.

#
drift_wind_aov <- aov(Wind.effect ~ Drift, data = geomorphology)
summary(drift_wind_aov)
# p-value = 2.86e-07 < 0.05, we refuse H0, Drift doesn't define wind effects.
