# Reset
rm(list = ls())

Etau <- 63
a <- 0.1767
s <- 2

traffic_factors = c(0.4, 0.7, 0.85, 0.925)

for (i in 1:length(traffic_factors)) {
	rho <- traffic_factors[i]

	# Theoretical values for the Weibull distribution
	## Weibull scale parameter b, depending on Etau, rho and a
	b <- (rho * Etau) / gamma((a + 1) / a)
	## Mean (E[x])
	weibull_mean <- b * gamma((a + 1) / a)
	## Variance
	weibull_variance <- b^2 * (gamma((a + 2) / a) - gamma((a + 1) / a)^2)
	## Coefficient of deviation (deviation/mean = sqrt(variance)/mean)
	weibull_coef_dev <- sqrt(weibull_variance) / weibull_mean

	# Mean interarrival rate
	lambda <- 1 / Etau
	# Mean service rate
	mu <- 1 / (s * weibull_mean)

	print(paste0("rho: ", rho, ", s: ", s, ", a: ", a, ", b: ", b))

	# Pollaczek-Khintchine's (P-K) formula (for M/G/1)
	Cx <- weibull_coef_dev
	PK_Lq <- ((rho^2) / (1 - rho)) * ((1 + Cx^2) / 2)
	PK_Wq <- PK_Lq / lambda
	PK_L <- PK_Lq + rho
	PK_W <- PK_L / lambda

	print(paste0("  M/G/1 Lq: ", PK_Lq))
	print(paste0("  M/G/1 Wq: ", PK_Wq))
	print(paste0("  M/G/1 L: ", PK_L))
	print(paste0("  M/G/1 W: ", PK_W))

	# Allen-Cunneen formula for GI/G/s
	theta <- s * rho
	A <- 0
	for (l in 0:(s-1)) {
		A <- A + (theta ^ l) / factorial(l)
	}
	B <- ((theta ^ s) / factorial(s)) * (1 / (1 - rho))
	C_s_theta <- B / (A + B)
	Lq_MMs <- C_s_theta * (rho / (1 - rho))

	exp_mean <- Etau
	exp_sd <- Etau # = sqrt(Etau ^ 2)
	Ctau <- Etau / Etau # should be 1

	Allen_Cuneen_Lq <- Lq_MMs * ((Ctau ^ 2 + Cx ^ 2) / 2)
	Allen_Cuneen_Wq <- Allen_Cuneen_Lq / (1 / Etau)
	Allen_Cuneen_L <- Allen_Cuneen_Lq + theta
	Allen_Cuneen_W <- Allen_Cuneen_L / lambda
	print(paste0("  M/G/s Lq: ", Allen_Cuneen_Lq))
	print(paste0("  M/G/s Wq: ", Allen_Cuneen_Wq))
	print(paste0("  M/G/s L: ", Allen_Cuneen_L))
	print(paste0("  M/G/s W: ", Allen_Cuneen_W))

	print(paste0("  -Mean interarrival rate (lambda): ",
				 lambda))
	print(paste0("  -Mean interarrival time (1/lambda): ",
				 1 / lambda))
	print(paste0("  -Variance of the interarrival times: ",
				 Etau ^ 2))
	print(paste0("  -Standard deviation of interarrival rate: ",
				 Ctau * lambda))
	print(paste0("  -Mean service rate (mu): ",
				 mu))
	print(paste0("  -Mean service time (1/mu): ",
				 1 / mu))
	print(paste0("  -Variance of the service times: ",
				 weibull_variance))
	print(paste0("  -Standard deviation of service rate: ",
				 Cx * mu))
	print(paste0("  -Number of parallel servers (c): ",
				 s))
	print(paste0("  -Ctau: ",
				 Ctau))
	print(paste0("  -Cx: ",
				 Cx))

	# For Latex
	print(paste0(rho, " & ", round(PK_L, 3), " & ", round(PK_Lq, 3), " & ",
				 round(Allen_Cuneen_L, 3), " & ", round(Allen_Cuneen_Lq, 3) , " \\"))
}
