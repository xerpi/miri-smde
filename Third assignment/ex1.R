# Reset
rm(list = ls())

library(moments)
source("sim.R")

set.seed(42)

# Parameters for our experiment and distribution
clients = 20000
a = 0.1767
b = 1

# Theoretical values for the Weibull distribution
## Mean (E[x])
theoretical_mean <- b * gamma((a + 1) / a)
## Variance
theoretical_variance <- b^2 * (gamma((a + 2) / a) - gamma((a + 1) / a)^2)
## Coefficient of deviation (deviation/mean = sqrt(variance)/mean)
theoretical_coef_dev <- sqrt(theoretical_variance) / theoretical_mean
## Kurtosis
#theoretical_kurtosis <- kurtosis()

# Weibull samples
samples_unif <- runif(clients)
samples <- uniform_to_weibull(a, b, samples_unif)

# Weibull samples values
sample_mean <- mean(samples)
sample_variance <- var(samples)
sample_coef_dev <- sd(samples) / sample_mean

# Exponential samples
exp_samples_unif <- runif(clients)
exp_samples <- uniform_to_exp(10, exp_samples_unif)

# Print information
sample_mean
theoretical_mean
sample_variance
theoretical_variance
sample_coef_dev
theoretical_coef_dev
kurtosis(samples)
kurtosis(exp_samples)

# !!! We have a lot of variance because it's heavy-tailed
# (param 'a' of Weibull very small)

# regular histogram (doesn't tell anything because long-tailed)
#hist(samples, breaks=200)

# Avoid "< 1" logarithm
samples <- samples + 1
exp_samples <- exp_samples + 1

# log-log scale histogram
h <- hist(samples, breaks=1000)
hist_x <- log(head(h$breaks, -1))
hist_y <- log(h$counts)
plot(hist_x, hist_y, main=paste("Weibull(a = ", a, ", b = ", b, ")"),
	 xlab="log(t+1)", ylab="log(freq(t+1))")

# Q-Q plot vs exponential distrib
#qqplot(exp_samples, samples, main="Q-Q - Exponential vs Weibull", cex=0.5)
log_samples <- sort(log(samples))
log_exp_samples <- sort(log(exp_samples))
plot(log_exp_samples, log_samples, main="Q-Q - Exponential vs Weibull",
	 xlab="sort(log(exp+1))", ylab="sort(log(weibull+1))", cex=0.75)

#bad qqline(samples, distribution = function(p) qweibull(p, a, b))
